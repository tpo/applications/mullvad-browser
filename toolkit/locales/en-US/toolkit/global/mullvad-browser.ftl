# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

## About Mullvad browser dialog.

mullvad-about-desc = { -brand-short-name }  is a privacy-focused web browser developed in collaboration between <label data-l10n-name="mullvad-about-mullvadLink">Mullvad VPN</label> and the <label data-l10n-name="mullvad-about-torProjectLink">Tor Project</label>. It’s produced to minimize tracking and fingerprinting.
mullvad-about-readMore = Read more
# Variables
#   $emailAddress (String) - The email address of Mullvad's support
mullvad-about-feedback2 = Help & feedback: { $emailAddress }
mullvad-about-telemetryLink = Telemetry

## Mullvad browser home page.

about-mullvad-browser-developed-by = Developed in collaboration between the <a data-l10n-name="tor-project-link">Tor Project</a> and <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Get more privacy by using the browser <a data-l10n-name="with-vpn-link">with Mullvad VPN</a>.
about-mullvad-browser-learn-more = Curious to learn more about the browser? <a data-l10n-name="learn-more-link">Take a dive into the mole hole</a>.

# Update message.
# <a data-l10n-name="update-link"> should contain the link text and close with </a>.
# $version (String) - The new browser version.
about-mullvad-browser-update-message =  { -brand-short-name } has been updated to { $version }. <a data-l10n-name="update-link">See what’s new</a>

## Home preferences.

home-mode-choice-mullvad =
    .label = { -brand-product-name } Home

## about:rights page.

rights-mullvad-intro = { -brand-short-name } is free and open source software.
rights-mullvad-you-should-know = There are a few things you should know:
rights-mullvad-trademarks =
    You are not granted any trademark rights or licenses to the trademarks of
    the { -brand-short-name } or any party, including without limitation the
    { -brand-short-name } name or logo.

## about:telemetry page.

telemetry-title = Telemetry Information
telemetry-description = Telemetry is disabled in { -brand-short-name }.
