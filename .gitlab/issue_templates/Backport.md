<!--
Title:
    Backport mullvad-browser#123: Title of Issue
    Backport Bugzilla 1234567: Title of Issue

This is an issue for tracking back-porting a patch-set (e.g. from Alpha to Stable or from Mozilla Rapid-Release to Alpha)
-->

## Backport Patchset

### Book-keeping

#### Issue(s)
- tor-browser#12345
- mullvad-browser#123
- https://bugzilla.mozilla.org/show_bug.cgi?id=1234567

#### Merge Request(s)
- mullvad-browser!123

#### Target Channels

- [ ] Alpha
- [ ] Stable

### Notes

<!-- whatever additional info, context, etc that would be helpful for backporting -->

/label ~"Apps::Type::Backport"
