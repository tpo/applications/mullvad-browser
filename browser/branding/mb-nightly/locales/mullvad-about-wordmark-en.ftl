# Should match the text in branding/content/about-wordmark.svg.
# Used for screen readers to represent the wordmark in text.
# NOTE: This should not be localised since the wordmark is not localised.
mullvad-about-wordmark-en = MULLVAD BROWSER NIGHTLY
